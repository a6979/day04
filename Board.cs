﻿using System.Collections.Generic;

namespace AdventOfCode4
{
	public class Board
	{
		public List<List<int>> Elements { get; set; }

		public Board(List<List<int>> elements)
		{
			Elements = elements;
		}
	}
}
