﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode4
{
	class Program
	{
		static void Main(string[] args)
		{
			var input = File.ReadAllText("E:\\Programming\\csharp\\AdventOfCode\\4\\AdventOfCode4\\input.txt");
			Console.WriteLine(SolvePart1(input));
			Console.WriteLine(SolvePart2(input));

			Console.ReadKey();
		}

		public static string SolvePart1(string input)
		{
			return CalculateScores(ParseNumbers(input), ParseBoards(input))
			.First()
			.ToString();
		}

		public static string SolvePart2(string input)
		{
			return CalculateScores(ParseNumbers(input), ParseBoards(input))
				.Last()
				.ToString();
		}

		static IEnumerable<int> CalculateScores(List<int> calledNumbers, List<List<int>> boards)
		{
			return calledNumbers.Select(n => boards
					.Where(b => !HasWon(b))
					.Select(b => CallNumber(n, b))
					.Where(b => HasWon(b))
					.Select(b => b.Where(cell => cell > 0).Sum() * n))
				.SelectMany(_ => _);
		}
			

		private static bool HasWon(List<int> board)
		{
			for (int r = 0; r < 5; ++r)
			{
				// if the row has won
				if (board.Skip(r * 5).Take(5).All(i => i < 0))
					return true;
			}

			// if any col has won
			for (int c = 0; c < 5; c++)
			{
				bool won = true;
				for (int i = 0; i < 5; ++i)
				{
					if (board[(i * 5) + c] > 0)
					{
						won = false;
						break;
					}
				}

				if (won)
					return true;
			}

			return false;
		}

		private static List<int> CallNumber(int n, List<int> board)
		{
			for (int i = 0; i < board.Count; ++i)
				if (board[i] == n)
					board[i] = -Math.Abs(n);

			return board;
		}

		private static List<int> ParseNumbers(string input)
		{
			return input.Substring(0, input.IndexOf('\n')).Split(",")
								.Select(int.Parse)
								.ToList();
		}
				

		private static List<List<int>> ParseBoards(string input)
		{
			return input.Substring(input.IndexOf('\n'))
				.Split("\n\n", StringSplitOptions.RemoveEmptyEntries)
				.Select(s => s
					.Replace("\n", " ")
					.Replace("  ", " ")
					.Trim()
					.Split(" ")
					.Select(int.Parse))
				.Select(e => e.ToList()).ToList();
		}
	}
}
